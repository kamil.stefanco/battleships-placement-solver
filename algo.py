import numpy as np
import matplotlib.pyplot as plt
import random
import copy


def create_plots(size, row_counts, col_counts):
    for i in range(10):
        grid = np.zeros((size, size), dtype=int)
        
        #row_counts = generate_ship_num(size, sum_ships)
        #col_counts = generate_ship_num(size, sum_ships)

        #plt.figure(figsize=(8, 6))
    
        # for i in range(size):
        #     plt.text(i, size, str(col_counts[i]), ha='center', va='center')
        #     plt.text(size, i, str(row_counts[i]), ha='center', va='center')

        # # Pridajte mriežku
        # plt.grid(which='both', color='grey', linestyle='-', linewidth=0.5)
        # plt.xticks(np.arange(-0.5, size + 1, 1), [])
        # plt.yticks(np.arange(-0.5, size + 1, 1), [])
        # #plt.show()
        
        return grid

def generate_ship_num(size,sum_ships):
    
    ships_row = np.full(size, 2)
    
    while sum(ships_row) != sum_ships:
        i = np.random.randint(0,size)
        if ships_row[i] == 0:
            continue
        ships_row[i] -= 1
    
    return ships_row


def check_touch(size, grid, x, y, ship_size, orientation):
    
    if x+ship_size>size and orientation=="horizontal":
        return True
    
    if y+ship_size>size and orientation=="vertical":
        return True
    
    if orientation == 'horizontal':
        for i in range(max(0, x - 1), min(size, x + ship_size + 1)):
            for j in range(max(0, y - 1), min(size, y + 2)):
                if grid[j][i] == 1:
                    return True
    else:
        for i in range(max(0, x - 1), min(size, x + 2)):
            for j in range(max(0, y - 1), min(size, y + ship_size + 1)):
                if grid[j][i] == 1:
                    return True
    return False


def is_won(grid,row_counts, col_counts):
    ships_in_rows = np.sum(grid,axis=1)
    ships_in_cols = np.sum(grid,axis=0) 
    
    return np.array_equal(ships_in_rows, row_counts) and np.array_equal(ships_in_cols, col_counts)


def plot_array(size, array, row_counts, col_counts):
    if not isinstance(array, np.ndarray):
        raise ValueError("Input must be a 2D array")

    size = array.shape[0]  

    plt.imshow(array, cmap='Blues', interpolation='none')

    for i in range(size):
        for j in range(size):
            plt.text(j, i, str(array[i, j]), ha='center', va='center', color='black')

    for i in range(size):
        plt.text(i, size, str(col_counts[i]), ha='center', va='center')
        plt.text(size, i, str(row_counts[i]), ha='center', va='center')

    plt.xticks([])  
    plt.yticks([])  

    color_bar = plt.colorbar()  
    color_bar.remove()  

    plt.show()

def can_be_placed(size, grid, x, y,length, orientation, row_counts, col_counts):
     
    
    if check_touch(size,grid, x, y, length, orientation):
        return False
    
    
    # iba vtedy ak je miesto pre lod v riadku podla napovedy
    if orientation == "horizontal":
        #najprv sa spytam či je dost miesta v riadku
        free_spots_row= row_counts[y]-np.sum(grid[y])
        if length>free_spots_row:
            return False
        
        # teraz sa pytam či je dost miesta v stlpcoch postupne
        for i in range(length):
            free_spots_col= col_counts[x+i]-np.sum(grid[:,x+i])
            if free_spots_col<1:
                return False
            
        
    # iba vtedy ak je miesto pre lod v stlpci podla napovedy    
    if orientation == "vertical":
        #najprv sa spytam či je dost miesta v riadku
        free_spots_col= col_counts[x]-np.sum(grid[:,x])
        if length>free_spots_col:
            return False
        
        # teraz sa pytam či je dost miesta v riadkoch postupne
        for i in range(length):
            free_spots_row= row_counts[y+i]-np.sum(grid[y+i])
            if free_spots_row<1:
                return False
            
    return True
    

def place_ship(grid,x,y,length,orientation):    
        
    # vkladanie lodiek    
    if orientation == "horizontal":
        for i in range(length):
            grid[y][x+i] = 1
    
    if orientation == "vertical":
        for i in range(length):
            grid[y+i][x] = 1



"""
##################################################
------------------------- DFS -------------------
##################################################
"""

def dfs(size,ships,row_counts,col_counts):
    
    start = create_plots(size,row_counts,col_counts)
    
    stack = [(start, ships)]
    
    stack_all = []
        
    while stack:
        
        current, current_ships = stack.pop()  
        
        # zoznam lodi je prazdny, sme na maximalnej hlbke
        if not current_ships:           
            if is_won(current, row_counts, col_counts):
                #plot_array(size, current, row_counts, col_counts)
                return current,len(stack_all)
            continue
        
        #nastavi aktualnu dlzku lode a odoberie ju zo zoznamu
        current_ship = current_ships[0]
        remaining_ships = current_ships[1:]
                      
        # vloží loď na všetky možné miesta a pridá ich do stacku
        for row in range(size):
            for col in range(size):
                if not check_touch(size, current, col, row, current_ship, "vertical"):
                    copy_current = copy.copy(current)
                    place_ship(copy_current, col, row, current_ship, "vertical")
                        
                    stack.append((copy_current, remaining_ships))
                    stack_all.append(copy_current)
                    #plot_array(size, copy_current, row_counts, col_counts)
                        
                if not check_touch(size, current, col, row, current_ship, "horizontal") and  current_ship != 1:
                    copy_current = copy.copy(current)
                    place_ship(copy_current, col, row, current_ship, "horizontal")

                    stack.append((copy_current, remaining_ships))
                    stack_all.append(copy_current)
                    #plot_array(size, copy_current, row_counts, col_counts)
                    
    return start,0 
                    


"""
##############################################################
------------------------- MRV BACKTRACKING -------------------
##############################################################
"""                                      

def mrv_backtracking(size,ships,row_counts,col_counts):
    
    start = create_plots(size,row_counts,col_counts)
    
    stack = [(start, ships)]
    
    stack_all = []
        
    while stack:
        
        current, current_ships = stack.pop()  
        
        # zoznam lodi je prazdny
        if not current_ships:           
            if is_won(current,row_counts, col_counts):
                return current,len(stack_all)
            continue
        
        # vyber lodku ktora ma najmenej moznych umiestneni
        current_ship = min(current_ships,key=lambda ship: count_possible_placements(size, current, ship, row_counts, col_counts))
        # ostatne lodky
        remaining_ships = current_ships.copy()
        remaining_ships.remove(current_ship)

        # vloží loď na všetky možné miesta a pridá ich do stacku
        for row in range(size):
            for col in range(size):
                if can_be_placed(size, current, col, row, current_ship, "vertical",row_counts, col_counts):
                    copy_current = copy.copy(current)
                    place_ship(copy_current, col, row, current_ship, "vertical")
                        
                    stack.append((copy_current, remaining_ships))
                    stack_all.append(copy_current)
                    #plot_array(size,copy_current, row_counts, col_counts)
                        
                    if is_won(copy_current,row_counts, col_counts):
                       return copy_current,len(stack_all) 
                            
                if can_be_placed(size, current, col, row, current_ship, "horizontal",row_counts, col_counts) and  current_ship != 1:
                    copy_current = copy.copy(current)
                    place_ship(copy_current, col, row, current_ship, "horizontal")

                    stack.append((copy_current, remaining_ships))
                    stack_all.append(copy_current)
                    #plot_array(size,copy_current, row_counts, col_counts)
                    
                    if is_won(copy_current,row_counts, col_counts):
                        return copy_current,len(stack_all)      
                    
    return start,0 
                    
def count_possible_placements(size, grid, ship_length, row_counts, col_counts):
    remaining = 0
    for row in range(size):
        for col in range(size):
            if can_be_placed(size, grid, col, row, ship_length, "vertical",row_counts, col_counts):
                remaining += 1

            if can_be_placed(size, grid, col, row, ship_length, "horizontal", row_counts, col_counts) and ship_length != 1:
                remaining += 1

    return remaining 
        
        
        
"""
##############################################################
------------------------- LCV BACKTRACKING -------------------
##############################################################
"""        
        
def lcv_backtracking(size,ships,row_counts,col_counts):
    
    start = create_plots(size,row_counts,col_counts)
    
    stack = [(start, ships)]
    
    stack_all = []

    while stack:
        current, current_ships = stack.pop()

        if not current_ships:
            if is_won(current,row_counts,col_counts):
                return current,len(stack_all)  
            continue
        
        #najde lod ktora ma najmensi pocet obmedzeni
        current_ship = find_least_constraining_ship(size, current, current_ships, row_counts, col_counts)
        remaining_ships = current_ships.copy()
        remaining_ships.remove(current_ship)

        # vloží loď na všetky možné miesta a pridá ich do stacku
        for row in range(size):
            for col in range(size):
                if can_be_placed(size, current, col, row, current_ship, "vertical",row_counts, col_counts):
                    copy_current = copy.copy(current)
                    place_ship(copy_current, col, row, current_ship, "vertical")
                        
                    stack.append((copy_current, remaining_ships))
                    stack_all.append(copy_current)
                    #plot_array(size,copy_current, row_counts, col_counts)
                        
                    if is_won(copy_current,row_counts, col_counts):
                        return copy_current,len(stack_all) 
                            
                if can_be_placed(size, current, col, row, current_ship, "horizontal",row_counts, col_counts) and  current_ship != 1:
                    copy_current = copy.copy(current)
                    place_ship(copy_current, col, row, current_ship, "horizontal")

                    stack.append((copy_current, remaining_ships))
                    stack_all.append(copy_current)
                    #plot_array(size,copy_current, row_counts, col_counts)
                    
                    if is_won(copy_current,row_counts, col_counts):
                        return copy_current,len(stack_all)   
                    
    return start,0 
        
              

def count_blocked_positions(size, grid, ship_length, row_counts, col_counts):
    blocked_positions = 0

    for row in range(size):
        for col in range(size):
            if not can_be_placed(size, grid, col, row, ship_length, "vertical", row_counts, col_counts):
                blocked_positions += 1

            if not can_be_placed(size, grid, col, row, ship_length, "horizontal", row_counts, col_counts) and ship_length != 1:
                blocked_positions += 1

    return blocked_positions

def find_least_constraining_ship(size, grid, current_ships, row_counts, col_counts):
    least_constraining_ship = None
    
    min_constraints = float('inf')

    for ship in current_ships:
        blocked_positions = count_blocked_positions(size, grid, ship, row_counts, col_counts)
        total_constraints = blocked_positions

        if total_constraints < min_constraints:
            min_constraints = total_constraints
            least_constraining_ship = ship

    return least_constraining_ship
       
        
    
    
        
"""
##############################################################
------------------------- FORWARD SEARCH MRV ---------------
##############################################################
"""        

# testuje ci sa do stavu zmestia vsetky lodky        
def can_place_ships_in_state(size ,state, ship_list,row_counts,col_counts):

    for ship in ship_list:
        placed = False
        
        for row in range(size):
            for col in range(size):
                if can_be_placed(size, state, col, row, ship, "vertical",row_counts, col_counts):
                    placed = True  
                    break

                if can_be_placed(size, state, col, row,ship, "horizontal",row_counts, col_counts) and ship != 1:
                    placed = True
                    break

        if not placed:
            return False

    return True


def mrv_forwardsearch(size,ships,row_counts,col_counts):
    
    start = create_plots(size,row_counts,col_counts)
    
    stack = [(start, ships)]
    
    stack_all = []
        
    while stack:
        
        current, current_ships = stack.pop()  
        
        # zoznam lodi je prazdny
        if not current_ships:           
            if is_won(current,row_counts, col_counts):
                return current,len(stack_all)
            continue
        
        # vyber lodku ktora ma najmenej moznych umiestneni        
        current_ship = min(current_ships,key=lambda ship: count_possible_placements(size, current, ship, row_counts, col_counts))
        #ostatne lodky
        remaining_ships = copy.deepcopy(current_ships)
        remaining_ships.remove(current_ship)
        
        if not can_place_ships_in_state(size, current, current_ships, row_counts, col_counts):
            continue
        
        # vloží loď na všetky možné miesta a pridá ich do stacku
        for row in range(size):
            for col in range(size):
                if can_be_placed(size, current, col, row, current_ship, "vertical",row_counts, col_counts):
                    copy_current = copy.deepcopy(current)
                    place_ship(copy_current, col, row, current_ship, "vertical")
                        
                    stack.append((copy_current, remaining_ships))
                    stack_all.append(copy_current)
                    #plot_array(size,copy_current, row_counts, col_counts)
                        
                    if is_won(copy_current,row_counts, col_counts):
                        return copy_current,len(stack_all)
                            
                if can_be_placed(size, current, col, row, current_ship, "horizontal",row_counts, col_counts) and  current_ship != 1:
                    copy_current = copy.deepcopy(current)
                    place_ship(copy_current, col, row, current_ship, "horizontal")

                    stack.append((copy_current, remaining_ships))
                    stack_all.append(copy_current)
                    #plot_array(size,copy_current, row_counts, col_counts)
                    
                    if is_won(copy_current,row_counts, col_counts):
                        return copy_current,len(stack_all)      
 
    return start,0                   
                    
"""
##############################################################
------------------------- FORWARD SEARCH LCV ---------------
##############################################################
"""                    
                    
def lcv_forwardsearch(size,ships,row_counts,col_counts):
    
    start = create_plots(size,row_counts,col_counts)
    
    stack = [(start, ships)]
    
    stack_all = []

    while stack:
        current, current_ships = stack.pop()

        if not current_ships:
            if is_won(current,row_counts,col_counts):
                return current,len(stack_all)  
            continue

        current_ship = find_least_constraining_ship(size, current, current_ships, row_counts, col_counts)
        remaining_ships = current_ships.copy()
        remaining_ships.remove(current_ship)
        
        if not can_place_ships_in_state(size, current, current_ships, row_counts, col_counts):
            continue

        # vloží loď na všetky možné miesta a pridá ich do stacku
        for row in range(size):
            for col in range(size):
                if can_be_placed(size, current, col, row, current_ship, "vertical",row_counts, col_counts):
                    copy_current = copy.copy(current)
                    place_ship(copy_current, col, row, current_ship, "vertical")
                        
                    stack.append((copy_current, remaining_ships))
                    stack_all.append(copy_current)
                    #plot_array(size,copy_current, row_counts, col_counts)
                        
                    if is_won(copy_current,row_counts, col_counts):
                        return copy_current,len(stack_all) 
                            
                if can_be_placed(size, current, col, row, current_ship, "horizontal",row_counts, col_counts) and  current_ship != 1:
                    copy_current = copy.copy(current)
                    place_ship(copy_current, col, row, current_ship, "horizontal")

                    stack.append((copy_current, remaining_ships))
                    stack_all.append(copy_current)
                    #plot_array(size,copy_current, row_counts, col_counts)
                    
                    if is_won(copy_current,row_counts, col_counts):
                        return copy_current,len(stack_all)                      
                    
    return start,0 