import copy
import numpy as np
import matplotlib.pyplot as plt
import tkinter as tk
from tkinter import ttk

import algo

class BattleshipGUI:
    def __init__(self, size, ships, row_counts, col_counts):
        self.size = size
        self.ships = ships
        self.row_counts = row_counts
        self.col_counts = col_counts

        self.root = tk.Tk()
        self.root.title("LODE")
        window_width = self.root.winfo_reqwidth()
        window_height = self.root.winfo_reqheight()
        position_right = int(self.root.winfo_screenwidth() / 2 - window_width / 2)
        position_down = int(self.root.winfo_screenheight() / 2 - window_height / 2)
        self.root.geometry(f"+{position_right}+{position_down}")

        self.fig, self.ax = plt.subplots()
        self.canvas = tk.Canvas(self.root)
        self.canvas.pack(side=tk.TOP, fill=tk.BOTH, expand=1)

        self.steps_label = tk.Label(self.root, text="Number of Steps: 0")
        self.steps_label.pack(side=tk.BOTTOM, padx=10, pady=10)

        self.start()

    def start(self):
        self.grid = algo.create_plots(self.size, self.row_counts, self.col_counts)
        self.update_game_board(0) 

        self.root.update_idletasks()
        window_width = self.root.winfo_width()
        window_height = self.root.winfo_height()
        position_right = int(self.root.winfo_screenwidth() / 2 - window_width / 2)
        position_down = int(self.root.winfo_screenheight() / 2 - window_height / 2)
        self.root.geometry(f"+{position_right}+{position_down}")
        
        self.create_buttons()
        
        self.root.mainloop()

    def draw_solution(self, solution):
        self.draw_game_board(solution)

    def plot_grid(self):
        self.canvas.delete("all")
        self.draw_game_board(self.grid)

    def draw_game_board(self, grid):
        
        cell_size = 50
        padding = 10

        board_width = self.size * cell_size
        board_height = self.size * cell_size

        for i in range(self.size + 1):
            x = i * cell_size + padding
            self.canvas.create_line(x, padding, x, board_height + padding, fill="black")
            y = i * cell_size + padding
            self.canvas.create_line(padding, y, board_width + padding, y, fill="black")

        for row in range(len(grid)):
            for col in range(len(grid[0])):
                x1 = col * cell_size + padding
                y1 = row * cell_size + padding
                x2 = x1 + cell_size
                y2 = y1 + cell_size

                if grid[row][col] == 1:
                    self.canvas.create_rectangle(x1, y1, x2, y2, fill="blue", outline="black")

        for i in range(len(self.row_counts)):
            self.canvas.create_text(cell_size * (len(grid[0]) + 0.5), i * cell_size + padding + cell_size / 2,
                                     text=str(self.row_counts[i]), anchor="w")
        for i in range(len(self.col_counts)):
            self.canvas.create_text(i * cell_size + padding + cell_size / 2, cell_size * (len(grid) + 0.5),
                                     text=str(self.col_counts[i]), anchor="n")

    def run_dfs(self):
        solution, steps = algo.dfs(self.size, self.ships, self.row_counts, self.col_counts)
        if is_solvable(steps):
            print("DFS naslo riesenie:")
            print("Steps:", steps)
            self.grid = solution
            self.update_game_board(steps)
            self.draw_solution(solution)

    def run_bt_lcv(self):
        solution, steps = algo.lcv_backtracking(self.size, self.ships, self.row_counts, self.col_counts)
        if is_solvable(steps):
            print("BT_LCV naslo riesenie:")
            print("Steps:", steps)
            self.grid = solution
            self.update_game_board(steps)

    def update_game_board(self,steps):
        self.canvas.delete("all")
        self.draw_game_board(self.grid)
        self.steps_label.config(text=f"Number of Steps: {steps}")


        
    def run_bt_mrv(self):
        solution, steps = algo.mrv_backtracking(self.size, self.ships, self.row_counts, self.col_counts)
        if is_solvable(steps):
            print("BT_MRV naslo riesenie:")
            print("Steps:", steps)
            self.grid = solution
            self.update_game_board(steps)


    def run_fs_mrv(self):
        solution, steps = algo.mrv_forwardsearch(self.size, self.ships, self.row_counts, self.col_counts)
        if is_solvable(steps):
            print("FS_MRV naslo riesenie:")
            print("Steps:", steps)
            self.grid = solution
            self.update_game_board(steps)


    def run_fs_lcv(self):
        solution, steps = algo.lcv_forwardsearch(self.size, self.ships, self.row_counts, self.col_counts)
        if is_solvable(steps):
            print("FS_LCV naslo riesenie")
            print("Steps:", steps)
            self.grid = solution
            self.update_game_board(steps)


    
    def run_statistics(self):
        dfs_solution, dfs_steps = algo.dfs(self.size, self.ships, self.row_counts, self.col_counts)
        mrv_bt_solution, mrv_bt_steps = algo.mrv_backtracking(self.size, self.ships, self.row_counts, self.col_counts)
        lcv_bt_solution, lcv_bt_steps = algo.lcv_backtracking(self.size, self.ships, self.row_counts, self.col_counts)
        mrv_fs_solution, mrv_fs_steps = algo.mrv_forwardsearch(self.size, self.ships, self.row_counts, self.col_counts)
        lcv_fs_solution, lcv_fs_steps = algo.lcv_forwardsearch(self.size, self.ships, self.row_counts, self.col_counts)

        self.plot_comparison_graph(
            ["DFS", "MRV Backtracking", "LCV Backtracking", "MRV Forward Search", "LCV Forward Search"],
            [dfs_steps, mrv_bt_steps, lcv_bt_steps, mrv_fs_steps, lcv_fs_steps])


    def plot_comparison_graph(self, algorithms, steps):
        plt.figure(figsize=(10, 6))
        bars = plt.bar(algorithms, steps, color=['blue', 'orange', 'green', 'red', 'purple'])
        plt.xlabel('Algoritmy')
        plt.ylabel('Počet krokov')
        plt.title('Porovnanie algoritmov')

        plt.xticks(algorithms, algorithms)

        for bar in bars:
            yval = bar.get_height()
            plt.text(bar.get_x() + bar.get_width() / 2, yval, round(yval, 2), ha='center', va='bottom')

        plt.show()

        
        
    def create_buttons(self):
        button_frame = ttk.Frame(self.root)
        button_frame.pack(side=tk.TOP, pady=10)

        buttons = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        for size in buttons:
            button = ttk.Button(button_frame, text=f"Pole {size}", command=lambda s=size: self.change_board_map(s))
            button.pack(side=tk.LEFT, padx=5)

        dfs_button = ttk.Button(self.root, text="DFS", command=self.run_dfs)
        dfs_button.pack(side=tk.LEFT, padx=10)

        bt_mrv_button = ttk.Button(self.root, text="Backtracking MRV", command=self.run_bt_mrv)
        bt_mrv_button.pack(side=tk.LEFT, padx=10)
    
        bt_lcv_button = ttk.Button(self.root, text="Backtracking LCV", command=self.run_bt_lcv)
        bt_lcv_button.pack(side=tk.LEFT, padx=10)

        fs_mrv_button = ttk.Button(self.root, text="Forward search MRV", command=self.run_fs_mrv)
        fs_mrv_button.pack(side=tk.LEFT, padx=10)

        fs_lcv_button = ttk.Button(self.root, text="Forward search LCV", command=self.run_fs_lcv)
        fs_lcv_button.pack(side=tk.LEFT, padx=10)

        stat_button = ttk.Button(self.root, text="Run statistics", command=self.run_statistics)
        stat_button.pack(side=tk.RIGHT, padx=10)           

    def change_board_map(self, pole):
        board_maps = {
            1: {"size": 3, "ships": [1, 2, 1], "row_counts": np.array([2, 1, 1]), "col_counts": np.array([2, 0, 2])},
            2: {"size": 5, "ships": [1,3,2,2,1], "row_counts": np.array([3,0,1,3,2]), "col_counts": np.array([2,1,3,1,2])},
            3: {"size": 5, "ships": [2,1,3,1,2], "row_counts": np.array([3,1,1,1,3]), "col_counts": np.array([2,3,1,2,1])},
            4: {"size": 6, "ships": [1,2,1,2,3,1], "row_counts": np.array([2,2,2,2,1,1]), "col_counts": np.array([2,2,0,3,0,3])},
            5: {"size": 6, "ships": [2,2,1,1,1,3], "row_counts": np.array([1,2,1,3,1,2]), "col_counts": np.array([2,2,2,2,1,1])},
            6: {"size": 6, "ships": [2,2,1,1,1,3], "row_counts": np.array([1,2,2,2,2,1]), "col_counts": np.array([1,3,0,3,0,3])},
            7: {"size": 6, "ships": [2,1,2,1,1,3], "row_counts": np.array([2,1,2,1,0,4]), "col_counts": np.array([4,1,3,0,1,1])},
            8: {"size": 8, "ships": [3,3,1,2,2,1,4,1,2,1], "row_counts": np.array([4,1,4,3,1,4,1,2]), "col_counts": np.array([3,2,4,1,5,1,3,1])},
            9: {"size": 10, "ships": [2,3,1,4,1,2,1,2,3,1], "row_counts": np.array([4,2,0,4,2,3,0,0,5,0]), "col_counts": np.array([6,1,2,1,0,2,3,0,1,4])},
            10: {"size": 5, "ships": [1,3,2,2], "row_counts": np.array([3,0,1,3,2]), "col_counts": np.array([2,1,3,1,2])},
            }

        if pole in board_maps:
            config = board_maps[pole]
            self.size = config["size"]
            self.ships = config["ships"]
            self.row_counts = config["row_counts"]
            self.col_counts = config["col_counts"]
            

        self.grid = algo.create_plots(self.size, self.row_counts, self.col_counts)
        self.update_game_board(0)

def is_solvable(steps):
    if steps == 0:
        print("Nemá riešenie")
        return False
    return True