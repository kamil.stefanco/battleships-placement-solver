import copy
import numpy as np

import algo

from gui import BattleshipGUI

if __name__ == "__main__":
    
    size = 5
    ships = [1,3,2,2,1]

    row_counts= np.array([3,0,1,3,2])
    col_counts= np.array([2,1,3,1,2])
    
    gui = BattleshipGUI(size, ships, row_counts, col_counts)



