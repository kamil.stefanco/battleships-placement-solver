# Battleships placement solver

## Description

Battleships placement solver is a program for automatically positioning a fleet of ships on a game board according to rules. The goal is to place the ships is such way that they dont touch each other (even at the corners) and that the number of cells occupied by ships matches the constrains for each row and column.

Utilizes DFS, backtracking and forward checking algorithms. Applies MRV (minimum remaining values) and LCV (least constraining values) approcahes for more efficient searching. Calculates the required steps to complete the task and compares results of each mentioned algorithm. 
